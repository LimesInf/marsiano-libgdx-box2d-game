package com.mygdx.game.backend.enums;

public enum Routes {
    CAR("/car/control"),
    SIGN_IN("/user/login");
    String route;
    Routes(String route) {
        this.route = route;
    }
    public String getRoute() {
        return route;
    }
}
