package com.mygdx.game.backend;


import com.mygdx.game.backend.secureConnection.SecurityManager;

import java.io.IOException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class HttpClient extends OkHttpClient {

    private static HttpClient httpClient;
    private static OkHttpClient okHttpClient;

    private HttpClient () {
        try {
            SecurityManager securityManager = new SecurityManager();
            okHttpClient = new OkHttpClient.Builder().sslSocketFactory(securityManager.getSslContext().getSocketFactory(), securityManager)
                    .hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String s, SSLSession sslSession) {
                    return true;
                }
            }).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static HttpClient getInstance()  {
        return okHttpClient == null ? httpClient = new HttpClient() : httpClient;
    }

    public void makeRequest(final RequestHandler requestHandler, final Request request){
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                requestHandler.fail(e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                requestHandler.success(response);
            }
        });
    }
}
