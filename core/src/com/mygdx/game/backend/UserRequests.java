package com.mygdx.game.backend;


import com.mygdx.game.backend.enums.Routes;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public final class UserRequests {
    public static void signIn(String username,String password,RequestHandler signInHandler) {
        String host = "34.211.232.248"; //change it

        int port = 8000;
        String url = "https://" + host + ":" + port + Routes.SIGN_IN.getRoute();

        JSONObject body = new JSONObject().put("username",username).put("password",password);
        RequestBody credentials = RequestBody.create(
                MediaType.parse("application/json"), body.toString());
        Request signInReq = new Request.Builder().url(url).post(credentials).build();
        HttpClient.getInstance().makeRequest(signInHandler,signInReq);
    }
}
