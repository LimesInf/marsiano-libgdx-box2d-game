package com.mygdx.game.backend.secureConnection;

import com.badlogic.gdx.Gdx;

import java.io.FileInputStream;
import java.net.URL;
import java.security.Key;
import java.security.KeyStore;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

public class SecurityManager implements X509TrustManager {

    private SSLContext sslContext;

    public SecurityManager() {
        try {

            String trustStoreLocataion = System.getenv("trustStoreLocation");
            String trustStorePassword = "changeit";

            String keyLocation = System.getenv("keyLocation");
            String keyPassword = "Har";

            KeyStore clientStore = KeyStore.getInstance("PKCS12");
            clientStore.load((Gdx.files.internal("keys/client.p12").read()),keyPassword.toCharArray());

            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(clientStore, keyPassword.toCharArray());
            KeyManager[] kms = kmf.getKeyManagers();

            sslContext = null;
            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(kms,new TrustManager[]{this} , new SecureRandom());

        }catch (Exception e) {
            e.printStackTrace();
        }

    }

    public SSLContext getSslContext() {
        return sslContext;
    }

    @Override
    public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

    }

    @Override
    public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return  new X509Certificate[0] ;
    }
}
