package com.mygdx.game.backend;

public final class CarRequestMaker {

    private CarRequestMaker() {

    }

    public static void doForwardRequest() {
        HttpClient.getInstance().makeRequest(CarRequests.Handlers.FORWARD_HANDLER.requestHandler,CarRequests.Control.FORWARD.request);
    }
    public static void doBackRequest() {
        HttpClient.getInstance().makeRequest(CarRequests.Handlers.BACK_HANDLER.requestHandler,CarRequests.Control.BACK.request);
    }
    public static void doLeftRequest() {
        HttpClient.getInstance().makeRequest(CarRequests.Handlers.LEFT_HANDLER.requestHandler,CarRequests.Control.LEFT.request);
    }
    public static void doRightRequest() {
        HttpClient.getInstance().makeRequest(CarRequests.Handlers.RIGHT_HANDLER.requestHandler,CarRequests.Control.RIGHT.request);
    }

}
