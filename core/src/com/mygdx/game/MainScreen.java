package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.backend.RequestHandler;
import com.mygdx.game.backend.UserRequests;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Response;


/**
 * Created by interstaller on 4/1/2018.
 */

public class MainScreen implements Screen {
    private Stage stage;
    private Game game;
    private static boolean isLoggedIn;

    private boolean isError;
    private Dialog wrongCredentialsDialog;
    private Dialog serverError;

    MainScreen(Game game) {

        this.game = game;
        isError = false;
        Viewport port = new FillViewport(IotControl.VIRTUAL_W, IotControl.VIRTUAL_H, new OrthographicCamera());
        stage = new Stage(port, ((IotControl) game).batch);
        Gdx.input.setInputProcessor(stage);
        Table table = new Table();
        Table inputTable = new Table();
        inputTable.right().top().padTop(30);
        inputTable.right().top().padTop(30);
        inputTable.setFillParent(true);
        table.center();
        table.setFillParent(true);

        Image menu = new Image(new Texture("menu.png"));
        Image tesla = new Image(new Texture("tesla.png"));

        final Drawable inputBox = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("input.png"))));
        final Drawable password = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("pass.png"))));

        BitmapFont bitmapFont = new BitmapFont();

        TextField.TextFieldStyle usernameField = new TextField.TextFieldStyle();
        usernameField.fontColor = Color.CYAN;
        usernameField.font = bitmapFont;
        usernameField.background = inputBox;

        TextField.TextFieldStyle passwordField = new TextField.TextFieldStyle();

        passwordField.fontColor = Color.CYAN;
        passwordField.font = bitmapFont;
        passwordField.background = password;

        final TextField usernameTextField = new TextField("", usernameField);
        final TextField passwordTextField = new TextField("", passwordField);

        passwordTextField.setPasswordMode(true);
        passwordTextField.setPasswordCharacter('*');
        usernameTextField.setAlignment(Align.center);
        passwordTextField.setAlignment(Align.center);
        usernameTextField.setMaxLength(12);
        passwordTextField.setMaxLength(20);

        inputTable.padTop(30);
        inputTable.add(usernameTextField).size(200,20);
        inputTable.row();
        inputTable.add(passwordTextField).size(200,20);
        inputTable.row();
        inputTable.add(tesla).size(200,50);

        Window.WindowStyle windowStyle = new Window.WindowStyle();
        windowStyle.titleFont = bitmapFont;

        windowStyle.titleFontColor = Color.FIREBRICK;

        wrongCredentialsDialog = new Dialog("Error",new Skin(Gdx.files.internal("uiskin.json")));
        wrongCredentialsDialog.text("wrong password or / and username");
        wrongCredentialsDialog.button("OK");
        tesla.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                String username = usernameTextField.getText();
                String pass = passwordTextField.getText();
                UserRequests.signIn(username,pass,signInHandler);
                return false;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

            }
        });


        table.add(menu);
        stage.addActor(table);
        stage.addActor(inputTable);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        stage.act(delta);

        if (isLoggedIn) {
            game.setScreen(new ConstrolScreen(stage));
            isLoggedIn = false;
            dispose();
        }

        if(isError) {
            wrongCredentialsDialog.show(stage);
            isError = false;
        } // implement dialog // implement server error dialog with button// change password //

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    RequestHandler signInHandler= new RequestHandler() {
        @Override
        public void success(Response response) {
            try {
                JSONObject resp = new JSONObject(response.body().string());
                System.out.println(resp.toString());
                if(resp.getJSONObject("response").optInt("status") == 200) {
                    isLoggedIn = true;
                } else {
                    isError = true;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void fail(Exception e) {

        }
    };
}
