package com.mygdx.game.backend;


import okhttp3.Request;
import okhttp3.Response;

public interface RequestHandler {
    void success(Response response);
    void fail(Exception e);
}
