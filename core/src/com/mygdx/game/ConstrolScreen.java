package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.mygdx.game.backend.CarRequestMaker;


public class ConstrolScreen implements Screen {

    private Stage stage;
    private Image leftButton;
    private Image rightButton;
    private Image upButton;
    private Image downButton;
    private Image backGround;
    private Table table;
    ConstrolScreen(Stage stage) {
        this.stage = stage;
        stage.clear();

        table = new Table();
        table.center();
        table.setFillParent(true);
        table.setBackground(new TextureRegionDrawable(new TextureRegion(new Texture("controlScreen.png"))));
        Table controlTable = new Table();
        Table directionTable = new Table();
        leftButton = new Image(new Texture(Gdx.files.internal("controlLeft.png")));
        rightButton = new Image(new Texture(Gdx.files.internal("controlRight.png")));
        upButton = new Image(new Texture(Gdx.files.internal("controlUp.png")));
        downButton = new Image(new Texture(Gdx.files.internal("controlDown.png")));


        leftButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                CarRequestMaker.doLeftRequest();
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

            }
        });

        rightButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                CarRequestMaker.doRightRequest();
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

            }
        });

        upButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                CarRequestMaker.doForwardRequest();
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

            }
        });

        downButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                CarRequestMaker.doBackRequest();
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

            }
        });
        controlTable.setSize( 200,60);
        directionTable.setSize(100,100);
        directionTable.right();
        directionTable.add(upButton);
        directionTable.row();
        directionTable.add(downButton);
        controlTable.add(leftButton).padRight(10);
        controlTable.add(rightButton);
        stage.addActor(table);
        stage.addActor(directionTable);
        directionTable.setPosition(stage.getWidth() - 100, 0);
        stage.addActor(controlTable);
    }
    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override

    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
