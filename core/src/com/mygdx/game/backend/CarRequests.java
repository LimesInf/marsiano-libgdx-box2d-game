package com.mygdx.game.backend;


import com.mygdx.game.backend.enums.Routes;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public final class CarRequests {

    private static final String HOST = "34.211.232.248"; //change it
    private static final int PORT = 8000;
    private final static String URL = "https://" + HOST + ":" + PORT + Routes.CAR.getRoute();

    private static Request doForward;
    private static Request doBack;
    private static Request doLeft;
    private static Request doRight;
    private static Request test;
    private static RequestHandler forwardHandler;
    private static RequestHandler backHandler;
    private static RequestHandler leftHandler;
    private static RequestHandler rightHandler;


    static {
        initHandlers();
        initRequests();
    }

    private static void initRequests() {

        String forwardData = "{\"direction\":\"forward\",\"speed\": 1}";
        String backData= "{\"direction\":\"back\",\"speed\": 1}";
        String leftData = "{\"direction\":\"left\",\"speed\": 1}";
        String rightData = "{\"direction\":\"right\",\"speed\": 1}";

        RequestBody forwardRequestBody = RequestBody.create(
                MediaType.parse("application/json"), forwardData);

        RequestBody backwardRequestBody = RequestBody.create(
                MediaType.parse("application/json"), backData);

        RequestBody leftRequestBody = RequestBody.create(
                MediaType.parse("application/json"), leftData);

        RequestBody rightRequestBody = RequestBody.create(
                MediaType.parse("application/json"), rightData);

        doForward = new Request.Builder().url(URL).post(forwardRequestBody).build();
        doBack = new Request.Builder().url(URL).post(backwardRequestBody).build();
        doLeft = new Request.Builder().url(URL).post(leftRequestBody).build();
        doRight = new Request.Builder().url(URL).post(rightRequestBody).build();
        test = new Request.Builder().url(URL).get().build();
    }

    private static void initHandlers() {

        forwardHandler = new RequestHandler() {
            @Override
            public void success(Response response) {
                try {
                    System.out.println("---- " + response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void fail(Exception e) {
                e.printStackTrace();
            }
        };

        leftHandler = new RequestHandler() {
            @Override
            public void success(Response response) {
                try {
                    System.out.println(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void fail(Exception e) {
                e.printStackTrace();
            }
        };

        backHandler = new RequestHandler() {
            @Override
            public void success(Response response) {
                try {
                    System.out.println(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void fail(Exception e) {
                e.printStackTrace();
            }
        };
        rightHandler = new RequestHandler() {
            @Override
            public void success(Response response) {
                try {
                    System.out.println(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void fail(Exception e) {
                e.printStackTrace();
            }
        };
    }

    private CarRequests() {

    }

    enum Control {
        FORWARD(doForward),BACK(doBack),LEFT(doLeft),RIGHT(doRight);
        Request request;
        Control(Request request) {
            this.request = request;
        }

        public Request getRequest() {
            return request;
        }
    }

    enum Handlers {
        FORWARD_HANDLER(forwardHandler),
        BACK_HANDLER(backHandler),
        LEFT_HANDLER(leftHandler),
        RIGHT_HANDLER(rightHandler);
        RequestHandler requestHandler;
        Handlers(RequestHandler requestHandler) {
            this.requestHandler = requestHandler;
        }

        public RequestHandler getRequestHandler() {
            return requestHandler;
        }
    }
}
